﻿using Microsoft.AspNetCore.Mvc;
using MiProyecto.DB;
using System.Security.Claims;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiProyecto.Controllers
{
    public class AuthController : Controller
    {

        private AppProyectoContext context;
        private IConfiguration configuration;
        public AuthController(AppProyectoContext context, IConfiguration configuration)
        {
            this.context = context;
            this.configuration = configuration;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(string username, string password)
        {
            var user = context.Usuarios
                    .FirstOrDefault(o => o.Username== username && o.Password == CreateHash(password));

            if (user == null)
            {
                TempData["AuthMessage"] = " Usuario o Password incorrecto!!";
                return RedirectToAction("Login");
            }

            // Autenticar
            var claims = new List<Claim> {
                new Claim(ClaimTypes.Name, user.Username),
            };

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

            HttpContext.SignInAsync(claimsPrincipal);


            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Login");
        }


        [HttpGet]
        public string Create(string password)
        {
            return CreateHash(password);
        }

        private string CreateHash(string input)
        {
            input += configuration.GetValue<string>("Key");
            var sha = SHA512.Create();

            var bytes = Encoding.Default.GetBytes(input);
            var hash = sha.ComputeHash(bytes);

            return Convert.ToBase64String(hash);
        }
    }
}

