﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MiProyecto.DB;
using MiProyecto.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MiProyecto.Controllers
{
    //[Authorize]
    public class AdminCursoController : Controller
    {
        private IWebHostEnvironment hosting;
        private AppProyectoContext context;

        public AdminCursoController(AppProyectoContext context, IWebHostEnvironment hosting)
        {
            this.hosting = hosting;
            this.context = context;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.Curso = context.Cursos.ToList();
            return View(new  Curso());
        }

        [HttpPost]
        public IActionResult Create(Curso curso, List<IFormFile> files)
        {
            if (ModelState.IsValid)
            {
                curso.Caratula = SaveFile(files[0]);

                context.Cursos.Add(curso);
                context.SaveChanges();


                return RedirectToAction("Create");
            }

            ViewBag.Curso = context.Cursos.ToList();
            return View();
        }



        [HttpGet]
        public IActionResult Edit(int id)
        {
            var curso = context.Cursos.Find(id);
            return View(curso);
        }

        [HttpPost]
        public IActionResult Edit(Curso curso, IFormFile file) // new Book {} con los datos nuevos
        {
            //book.Nombre = "este nombre tambien quiereo que se guarade"; // esto no hace nada en la base de datos

            var curso_db = context.Cursos.Find(curso.Id); // new Book {} -> conexión con la base de datos
            curso_db.Titulo = curso.Titulo;
            curso_db.Id = curso.Id;
            //context.Books.Update(book); // asegurse de enviar todos los datos
            context.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            var curso = context.Cursos.Find(id);
            context.Cursos.Remove(curso);

            context.SaveChanges();

            return RedirectToAction("Index");
        }


        private string SaveFile(IFormFile file)
        {
            string relativePath = "";

            if (file.Length > 0 && file.ContentType == "image/png")
            {
                relativePath = Path.Combine("files", file.FileName);
                var filePath = Path.Combine(hosting.WebRootPath, relativePath);
                var stream = new FileStream(filePath, FileMode.Create);
                file.CopyTo(stream);
                stream.Close();
            }

            return "/" + relativePath.Replace('\\', '/');
        }
    }
}