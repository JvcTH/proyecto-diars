﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MiProyecto.DB;

namespace MiProyecto.Controllers
{
    public class HomeController : Controller
    {
        private AppProyectoContext context;
        public HomeController(AppProyectoContext context)
        {
            this.context = context;
        }

        //[Authorize]
        public IActionResult Index()
        {
           var cursos = context.Cursos.Include(o => o.CursoProfesores).ThenInclude(o => o.Profesor).ToList();
          //  var cursos = context.Cursos.ToList();
            return View("Index", cursos);
        }
    }
}
