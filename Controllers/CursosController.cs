﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MiProyecto.DB;
using MiProyecto.Models;

namespace MiProyecto.Controllers
{
    // La clase cursoDetalle solo tiene un Curso y una lista de CursoProfesor
    // Dentro de cursoProfesor yo puedo acceder a cada uno de los profesores
    public class CursoDetalle
    {
        public Curso Curso { get; set; }
        public List<Profesor> Profesores { get; set; }
    }
    
    public class CursosController : Controller
    {
        private AppProyectoContext context;

        public CursosController(AppProyectoContext context)
        {
            this.context = context;
        }

       
        public IActionResult Detalle(int id)
        {
            // Obtengo el curso por su ID
            var curso = context.Cursos.FirstOrDefault(o => o.Id == id);

            // Obtengo todos los cursosDetalle que coincidan con el CursoID
            var cursoProfesores = context.CursoProfesores.Where(o => o.CursoId == curso.Id).Include(o => o.Profesor).ToList(); //Envia como null

            var profesores = new List<Profesor>();

            foreach (var cursoProfesor in cursoProfesores)
            {
                var profesor = context.Profesores.FirstOrDefault(o => o.Id == cursoProfesor.ProfesorId);
                profesores.Add(profesor);
            }

            // Creo un objeto de la clase CursoDetalle para enviar por Model los datos
            var cursoDetalle = new CursoDetalle
            {
                Curso = curso,
                Profesores = profesores
            };
            return View(cursoDetalle);
        }
        
    }
}
