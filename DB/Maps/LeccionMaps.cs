﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiProyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiProyecto.DB.Maps
{
    public class LeccionMaps : IEntityTypeConfiguration<Leccion>
    {
        public void Configure(EntityTypeBuilder<Leccion> builder)
        {

            builder.ToTable("Leccion");
            builder.HasKey(book => book.Id);
        }
    }
}
